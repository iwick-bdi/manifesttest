val common = (project in file("common"))
  .settings(name := "common",
    Compile / packageBin / packageOptions +=
    Package.ManifestAttributes("Implementation-Version" -> "common implementation"))

val moduleA = (project in file("moduleA"))
  .dependsOn(common)
  .settings(name := "moduleA",
    Compile / packageBin / packageOptions +=
    Package.ManifestAttributes("Implementation-Version" -> "moduleA implementation"))

val manifesttest = (project in file(".")).
  settings(name := "manifesttest").
  aggregate(common, moduleA)
