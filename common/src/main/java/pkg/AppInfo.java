package pkg;

public class AppInfo {
    public static String getAppInfo() {
        return AppInfo.class.getPackage().getImplementationVersion();
    }
}
