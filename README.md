https://biodiscovery.atlassian.net/browse/NC-5576?focusedCommentId=78667

See implementation of `java.lang.ClassLoader#getPackage`

It has a HashMap of `packages` it has already loaded, and will return the cached one, even if it loads a class from a different jar with a different manifest.

```shell
$ sbt package

# note semicolons in classpath because this was on windows

$ java -cp common/target/scala-2.12/common_2.12-0.1.0-SNAPSHOT.jar\;moduleA/target/scala-2.12/modulea_2.12-0.1.0-SNAPSHOT.jar pkg.MainPkg
moduleA implementation

$ java -cp common/target/scala-2.12/common_2.12-0.1.0-SNAPSHOT.jar\;moduleA/target/scala-2.12/modulea_2.12-0.1.0-SNAPSHOT.jar pkg.other.MainOther
common implementation
```
